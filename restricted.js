var restricted = {
  "PYROMANCER'S CACHE": "j",
  "FURY OF THE WOLF": "j",
  "FURY OF THE LION": "j",
  "FURY OF THE STAG": "j, m",
  "FURY OF THE KRAKEN": "j",
  "FURY OF THE DRAGON": "j",
  "FURY OF THE SUN": "j",
  "CASTELLAN OF THE ROCK": "j",
  "KINGS OF SUMMER": "j",
  "KINGS OF WINTER": "j",
  "GAME OF CYVASSE": "j",
  "BOLTON REFUGEE": "j",
  "MOUNTAIN REFUGEE": "j",
  "HIGHGARDEN REFUGEE": "j",
  "ISLAND REFUGEE": "j",
  "REFUGEE OF THE PLAINS": "j",
  "REFUGEE OF THE CITADEL": "j",
  "VALE REFUGEE": "j",
  "STREET WAIF": "j",
  "ROBERT BARATHEON": "j; TTotH",
  "VENOMOUS BLADE": "j",
  "FEAR OF WINTER": "j, m",
  "THE HATCHLING'S FEAST": "j",
  "BURNING ON THE SAND": "j",
  "VAL": "j, m",
  "ORPHAN OF THE GREENBLOOD": "j",
  "THE VIPER'S BANNERMEN": "j, m",
  "NARROW ESCAPE": "j, m",
  "RETALIATION!": "j",
  "THE MAESTER'S PATH ": "j, m",
  "THE CONCLAVE": "j, m",
  "TIN LINK": "j",
  "SEARCH AND DETAIN": "j",
  "THREAT FOM THE EAST": "j",
  "MEERA REED": "j",
  "THE SCOURGE": "j, m",
  "ASHA GREYJOY": "j, m; WLL",
  "MANNING THE CITY WALLS": "j",
  "THE LONG LANCES": "j",
  "NEGOTIATIONS AT THE GREAT SEPT": "j",
  "WAR OF FIVE KINGS": "m",
  "NORTHERN CAVALRY FLANK": "m",
  "BATTLE OF THE BAY": "m",
  "THE RED VIPER": "m; PotS",
  "MAKE AN EXAMPLE": "m",
  "SUPERIOR CLAIM": "m",
  "HELLHOLT ENGINEER": "m",
  "THE ART OF SEDUCTION": "m",
  "CORPSE LAKE": "m",
  "BATTLE FOR THE SHIELD ISLANDS": "m"
};

if (window.location.href.indexOf("agotcards.org") > -1) {
  var markRestricted = function(linkNode) {
    var restrictionKey = linkNode.innerText.toUpperCase().trim(),
        restriction = restricted[restrictionKey];

    if (!restriction) {
      return;
    }

    linkNode.innerHTML = linkNode.innerHTML + " (<span style='color:red;'>" + restriction + "</span>)";
  };
  var linkNodes = document.body.getElementsByTagName("a");
  for (var i = 0; i < linkNodes.length; i++) {
    markRestricted(linkNodes[i]);
  }

  var titleNodes = document.body.querySelectorAll("span.spoil_title");
  for (i = 0; i < titleNodes.length; i++) {
    markRestricted(titleNodes[i]);
  }

} else {
  var body = document.body.getElementsByClassName("ipsBox")[0] || document.body.getElementsByClassName("content")[0];
  for (var card in restricted) {
    var notes = restricted[card],
        replacement = "$& (<span style='color:red;'>" + notes + "</span>)";
    body.innerHTML = body.innerHTML.replace(new RegExp("[\\s>]" + card + "\\b", "gi"), replacement);
  }
}
