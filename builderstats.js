// add a "view" item to the toolbar
// #toolbar ul > li

// element_wrapper
// > input[rel='card_type']
// > .element:visible .element_amount .text
(function($) {
  var viewUrl = document.location.href.replace(/\/build\//, '/v/'),
      deckId = viewUrl.replace(/.*\//, '');
  $('#toolbar ul')
    .append("<li><a href='" + viewUrl + "'>View</a></li>")
    .append('<a href="/api/download_deck/' + deckId + '" title="Export deck list" id="export_button"><img alt="" src="/images/16/export.png" class="">Export</a>');

  var countCards = function() {
    var cardBoxes = $('.element_wrapper');
    var totals = { total: 0 };
    var exclude = ["Plot", "Agenda", "House Card"];

    cardBoxes.each(function(idx, boxEl) {
      var box = $(boxEl),
          type = box.find('input[rel=card_type]').attr('value'),
          count = parseInt(box.find('.element:visible .element_amount').text() || '0', 10);

      totals[type] = (totals[type] || 0) + count;
      if (exclude.indexOf(type) === -1) {
        totals['total'] += count;
      }
    });
    return totals;
  };

  var updateTotals = function() {
    var statBox = $('#statBox'),
        totals = countCards(),
        types = ["Character", "Event", "Attachment", "Location"];
    var statHtml = types.map(function(type) {
      return "<b>" + type + "s:</b> " + (totals[type] || 0);
    }).join('<br/>') + "<br/><b>Total:</b> " + (totals['total'] || 0);
    statBox.html(statHtml);
  };

  var boxShadowStyle = '0px 4px 10px #999999';
  var statBox = $('<div>')
    .css({
      'position': 'fixed',
      'top': '80px', // TODO: smarter placement
      'left': '5px'
    })
    .append($('<div>').attr('id', 'statBox')
      .css({
        'padding': '5px',
        'background-color': 'white',
        'border': '1px solid lightgray',
        '-webkit-box-shadow': boxShadowStyle,
        '-moz-box-shadow': boxShadowStyle,
        'box-shadow': boxShadowStyle
      })
    );

  $(function() {
    $(document.body).append(statBox);

    // setup the mutation observer
    var cardsArea = document.getElementById('deck_cards'),
        observer = new MutationObserver(function(mutations) {
          updateTotals();
        });

    observer.observe(cardsArea, { subtree: true, childList: true, attributes: true, attributeFilter: ['style'] });
  });
})(jQuery);