(function($) {
  var hover_card_badge = false;
  var cachedCards = {};

  var initHover = function() {
    $('a[cardid]').unbind('mouseenter mouseleave');

    $('a').filter(function(idx, e) { return ($(e).attr('href') || '').match(/^\/card\//); })
      .each(function(idx, e) {
        var link = $(e),
            cardid = link.attr('href').replace(/.*card.*\//, '');
        link.attr('cardid', cardid);
      });

    $('a[cardid]').hover(fetch_badge, close_badge);
  };

  $(document).bind('mouseover.firstHover', function(e) {
    var target = e.target,
        href = target.attributes['href'];
    if (target.tagName === 'A' && href && href.value.indexOf('/card/') > -1) {
      console.log("init hover");
      initHover();
      $(document).unbind('mouseover.firstHover');
      fetch_badge(e);
    }
  });

  $('.card_over_badge').mouseenter(function() {
    hover_card_badge = true;
  });

  $('.card_over_badge').mouseleave(function() {
    hover_card_badge = false;
  });

  function close_badge() {
    if (!hover_card_badge) {
      $('.card_over_badge').remove();
    }
  }

  function fetch_badge(event) {
    var target = $(event.target),
        id = target.attr('cardid');
    if (!id) {
      console.log("can't find id. target:");
      console.log(target);
      return;
    }

    if (cachedCards[id]) {
      show_badge_popup(cachedCards[id], id, target);
      return;
    }

    $.ajax( {
      url : "/deck/card_hover_badge/" + id,
      global : false,
      type : "POST",
      data : ( {
        id : id
      }),
      dataType : "html",
      async : true,
      success : function(data) {
        if (data !== "") {
          cachedCards[id] = data;
          show_badge_popup(data, id, target);
        }
      },
      error : function(data) {
        alert('Something that should never fail has failed: ' + data.response);
      }
    });
  }

  function show_badge_popup(item, id, link)
  {
    close_badge();

    item = item.replace('<img', '<div'); // prevent loading the images
    $('body').append(item);
    var linkPosition = link.offset(),
        popup = $('.card_over_badge[cardid=' + id + ']');

    popup.css('position', 'absolute');
    popup.css('left', linkPosition.left - 10);
    popup.css('top', linkPosition.top + 50);

    popup.removeClass('invisible');

    // HACK FOR SPOIL_ITEM BG COLOR AND STUFF
    var spoil_item = $('.card_over_badge[cardid=' + id + '] .spoil_item');
    var boxShadowStyle = '0px 4px 10px #999999';
    spoil_item.css('background-color', 'white');
    spoil_item.css('border', '1px solid lightgray');
    spoil_item.css('-webkit-box-shadow', boxShadowStyle);
    spoil_item.css('-moz-box-shadow', boxShadowStyle);
    spoil_item.css('box-shadow', boxShadowStyle);
  }

})(jQuery);